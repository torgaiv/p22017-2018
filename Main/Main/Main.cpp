/*---------------------------------------------------------------------------------------------------------------------------------------------------------
 * - Pagina de la asignatura:	http://www.dlsi.ua.es/asignaturas/p2/
 * - Enunciado de la práctica:	http://www.dlsi.ua.es/asignaturas/p2/downloads/1718/practicas/pr1/prog2p1-es.pdf
 * - Archivo .cc original:		http://www.dlsi.ua.es/asignaturas/p2/downloads/1718/practicas/pr1/evacuationOfHoth.cc
---------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------------------------------------------
	====================================================
	=====		 INFORMACION | NOTAS			========
	====================================================
	
	-Para leer strings de teclado estoy usando:
		getline(cin, string, '\n');
	 esta función lee hasta el retorno de carro
	 ignorando el resto, de esta forma no es necesario
	 limpiar el búffer
	 -Para limpiar el búffer estoy usando:
		cin.ignore()
	  que ignora la nueva linea que 
	  crea cin >> char / string / int

*/

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <cmath>
#include <cfloat>
#include <string>
using namespace std;

struct Base {
	string name;
	int people;
	int equipment;
	double x, y;
};

struct Ship {
	string name;
	int maxPeople;
	int maxEquipment;
	int curPeople;
	int curEquipment;
	vector<Base> bases;
};

struct Planet {
	string name;
	vector<Base> bases;
	vector<Ship> ships;
};


// ---------------- error -------------------------

enum Error {
	ERR_UNKNOWN_OPTION,
	ERR_WRONG_NUMBER,
	ERR_LOW_PEOPLE,
	ERR_WRONG_BASE_NAME,
	ERR_WRONG_SHIP_NAME,
	ERR_NO_BASES,
	ERR_NO_SHIPS
};

void error(Error n)
{
	switch (n) {

	case ERR_UNKNOWN_OPTION:
		cout << "Error, unknown option" << endl;
		break;

	case ERR_WRONG_NUMBER:
		cout << "Error, wrong number, must be positive" << endl;
		break;

	case ERR_LOW_PEOPLE:
		cout << "Error, low people number" << endl;
		break;

	case ERR_WRONG_BASE_NAME:
		cout << "Error, base name not found" << endl;
		break;

	case ERR_WRONG_SHIP_NAME:
		cout << "Error, wrong ship name" << endl;
		break;

	case ERR_NO_BASES:
		cout << "Error, there are no bases in the planet" << endl;
		break;

	case ERR_NO_SHIPS:
		cout << "Error, there are no ships in the planet" << endl;
		break;
	}
}


// ------------------------------------------------------------------
/*	A la hora de mostrar por pantalla una base, se mostrara con el siguiente
*	formato: [(main base) 1251 12345 (12.34,-7.89)]
*	Entre parentesis se pondra el nombre de la base ("main base"),
*	a continuacion el numero de personas (\1251"),
*	la cantidad de material que se debe evacuar (\12345"),
*	y finalmente las coordenadas entre parentesis (\(12.34,-7.89)").
*/
void printBase(const Base &base) // prints base info
{
	cout << "[(" << base.name << ") " << base.people << " " << base.equipment << " (" << base.x << "," << base.y << ")]" << endl;
}
/*
La informacion de la nave se mostrara en pantalla como en el siguiente
	Ejemplo :
	[(Corellian cruiser Princess Leia) 300{0} 45000{0}]
Donde primero aparece el nombre de la nave entre parentesis, el numero
maximo de personas, seguido del valor del campo "curPeople" entre llaves,
y la cantidad maxima de material seguida del campo "curEquipment" entre
llaves.
*/
void printShip(const Ship &ship) // prints ship info
{
	cout << "[(" << ship.name << ") " << ship.maxPeople << "{" << ship.curPeople << "} " << ship.maxEquipment << "{" << ship.curPeople << "}]" << endl;
}

/*
La informacion que se debe mostrar del planeta cuando el usuario elija la
opcion List info del menu es, por ejemplo :
		Planet: Hoth
		Ships :
		[(Corellian cruiser Princess Leia) 300{0} 45000{0}]
		[(Cargo ship Alderaan) 5000{0} 15000{0}]
		[(Cargo ship D'Qar) 600{0} 12500{0}]
		Bases:
		[(main base) 2500 25670 (0.676, -2.43)]
		[(logistic base) 259 8765 (12.22, -7.56)]
		[(dorm base) 78 3567 (13.05, 4.56)]
Lo primero que aparece es el nombre del planeta("Hoth" en el ejemplo),
la informacion de las naves, y nalmente la informacion de las bases.Cuando
se elige la opcion List info se muestra la informacion como en el ejemplo
anterior; sin embargo, cuando se muestra la informacion del planeta mientras
se esta aplicando el algoritmo para la evacuacion, las naves pueden tener una
o mas bases asignadas que deben mostrarse junto con el resto de informacion
de la nave.Mas adelante, en la seccion 2.7, se muestra un ejemplo con bases
asignadas a una nave.
*/
void printPlanet(const Planet &planet) // prints planet info
{
	cout << "Planet: " << planet.name << endl;
	cout << "Ships :" << endl;
	for (unsigned int i = 0; i < planet.ships.size(); i++) {
		printShip(planet.ships[i]);
	}
	cout << "Bases:" << endl;
	for (unsigned int i = 0; i < planet.bases.size(); i++) {
		printBase(planet.bases[i]);
	}
}

// Checks if the basename exists
// Return base id if exists -1 otherwise
int BaseExist(string baseName, Planet &planet)
{
	int exists = -1;
	for (unsigned int i = 0 && exists != -1; i < planet.bases.size(); i++)
	{
		if (planet.bases[i].name == baseName) {
			exists = i;
		}
	}
	return exists;
}

//Para calcular la distancia entre dos puntos(x1, y1) y(x2, y2) se debe usar la distancia euclídea
double CalculaDistancia(double puntoXA, double puntoYA, double puntoXB, double puntoYB)
{
	/*
		d = sqrt [(x1 −x2)^2 + (y1 −y2)^2]
	*/
	return sqrt(pow(puntoXA - puntoXB, 2.0) + pow(puntoYA - puntoYB, 2.0));
}

// Creates/modifies a base
void createBase(Planet &planet)
{
	/*Para añadir una nueva base al planeta, en la opcion Add / modify base, se debe llamar a la funcion createBase, en la que se debe pedir al usuario el
	nombre de la base con el siguiente mensaje :
	Enter base name :*/
	string baseName = "";
	cout << "Enter base name: ";
	getline(cin, baseName, '\n');
	int baseId = BaseExist(baseName, planet);
	/*Por otro lado, si al introducir el nombre de la base resulta que la base ya
	 existe en el planeta, se mostraran los datos de la base y se preguntara al usuario
	 si desea modificarlos :
		 [(main base) 2500 25670 (0.676, -2.43)]
		 Modify base data(Y / N) ?
	*/
	if (baseId != -1) {
		printBase(planet.bases[baseId]);
		char answer = 'N';
		cout << "Modify base data(Y / N) ?:" << endl;
		cin >> answer; cin.ignore();
		/*Si el usuario introduce el caracter \Y" (en mayuscula), se entendera que
		desea modificar la base, y si introduce cualquier otro caracter(\N" o cualquier
		otro), se entendera que no quiere modicarla y se volvera al menu principal.
		*/
		if (answer == 'Y') {
			/*Para modificar la base se debe pedir al usuario el dato a modificar :
			Which base data(p / e / c) ?
			Si se introduce una p" se pedira el numero de personas (como se explica mas
			arriba), si se introduce 'e' se pedira la cantidad de material, y si se introduce
			'c' se pediran las coordenadas. Si el numero de personas o la cantidad de
			material son incorrectos, se emitira el error correspondiente y no se modicara
			la base.
			*/
			cout << "Which base data(p / e / c) ?:";
			cin >> answer;
			cin.ignore();
			if (answer == 'p') {

				int basePeople = 0;
				cout << "Enter base people: ";
				cin >> basePeople;
				cin.ignore();
				if (basePeople <= 0) {
					error(ERR_WRONG_NUMBER);
				}
				else if (basePeople <= 50) {
					error(ERR_LOW_PEOPLE);
				}
				else {
					planet.bases[baseId].people = basePeople;
				}
			}
			else if (answer == 'e') {

				int equipment = 0;
				cout << "Enter base equipment(Kg) :";
				cin >> equipment;
				cin.ignore();
				if (equipment <= 0) {
					error(ERR_WRONG_NUMBER);
				}
				else {
					planet.bases[baseId].equipment = equipment;
				}
			}
			else if (answer == 'c') {
				double cordX = 0.0;
				double cordY = 0.0;
				cout << "Enter base coordinate x :";
				cin >> cordX;
				cin.ignore();
				cout << "Enter base coordinate y :";
				cin >> cordY;
				cin.ignore();
				planet.bases[baseId].x = cordX;
				planet.bases[baseId].y = cordY;
			}
			/*Finalmente, si el caracter introducido no es ninguno de los posibles 'p','e','c',
			se emitira el error ERR_UNKNOWN_OPTION y se volvera al menu sin
			modificar la base.Si no hay errores en los datos de entrada, se modificara el
			dato correspondiente de la base del planeta, y se volvera al menu.
			*/
			else {
				error(ERR_UNKNOWN_OPTION);
			}
		}
		else
		{
		}
	}
	else {

		/*	Si el nombre de la base no existe, se pedira a continuacion los datos de la base, empezando por el numero de personas :
			Enter base people :
		*/
		int basePeople = 0;
		cout << "Enter base people: ";
		cin >> basePeople;
		cin.ignore();

		/*Si el numero de personas es menor o igual que 0, el programa debe emitir el error ERR_WRONG_NUMBER,
		y salir de la funcion(o funciones) hasta volver al
		bucle del menu principal, sin añadir la base al planeta.
		*/

		if (basePeople <= 0) {
			error(ERR_WRONG_NUMBER);
		}

		/*Si no se emite dicho error, a continuacion se comprobara si el numero de personas es menor que 50
		y en ese caso se emitira el error ERR_LOW_PEOPLE y se saldra de la funcion.
		*/

		else if (basePeople <= 50) {
			error(ERR_LOW_PEOPLE);
		}

		/*Si el numero de personas es correcto, se pedira la cantidad de material :
			Enter base equipment(Kg) :
		Como en el caso del numero de personas, si la cantidad introducida es menor
		o igual que 0 se emitira el error ERR_WRONG_NUMBER y se volvera al menu
		principal.
		*/

		else {
			int equipment = 0;
			cout << "Enter base equipment(Kg) :";
			cin >> equipment;
			cin.ignore();
			if (equipment <= 0) {
				error(ERR_WRONG_NUMBER);
			}
			else {

				/*Si la cantidad es correcta, se pediran las coordenadas de la base con los
				siguientes mensajes :
					Enter base coordinate x :
					Enter base coordinate y :
				No es necesario ltrar las coordenadas, cualquier numero es valido como
				coordenada.
				*/

				double cordX = 0.0;
				double cordY = 0.0;
				cout << "Enter base coordinate x :";
				cin >> cordX;
				cin.ignore();
				cout << "Enter base coordinate y :";
				cin >> cordY;
				cin.ignore();

				/*Una vez introducidos todos los datos, se a~nadira la base al nal del
				  vector de bases del planeta.
				*/
				Base newBase;
				newBase.equipment = equipment;
				newBase.name = baseName;
				newBase.people = basePeople;
				newBase.x = cordX;
				newBase.y = cordY;
				planet.bases.push_back(newBase);
			}
		}
	}

}

// Checks if the ship exists
// Return ship id if exists, -1 otherwise
int ExistShip(string shipName, Planet &planet) {
	int shipId = -1;
	for (unsigned int i = 0 && shipId != -1; i < planet.ships.size(); i++)
	{
		if (planet.ships[i].name == shipName) {
			shipId = i;
		}
	}
	return shipId;
}

// Creates a ship
/*
Cuando el usuario elige la opcion Add ship del menu, el programa debe llamar
a la funcion createShip, que pedira el nombre de la nave con este mensaje :
	Enter ship name :
*/
void createShip(Planet &planet)
{
	string shipName = "";
	cout << "Enter ship name : ";
	getline(cin, shipName, '\n');
	int shipId = ExistShip(shipName, planet);

	if (shipId != -1) {
		/*Si el nombre de la nave no existe, se pediran el numero maximo de personas
		y la cantidad maxima de material con los siguientes mensajes :
			Enter ship maximum people :
			Enter ship maximum equipment :
			Como en la introduccion de datos de la base, si el numero maximo de personas
			no es mayor que 0 se debe emitir el error ERR_WRONG_NUMBER y volver
			al menu (sin llegar a pedir la cantidad maxima de material); despues, si el
			numero introducido es mayor que 0 pero es menor que 100 se emitira el error
			ERR_LOW_PEOPLE y se volvera al menu.
		*/
		int maxPeople = 0;
		cout << "Enter ship maximum people : ";
		cin >> maxPeople;
		cin.ignore();

		if (maxPeople <= 0) {
			error(ERR_WRONG_NUMBER);
		}
		else if (maxPeople < 100) {
			error(ERR_LOW_PEOPLE);
		}
		else {
			/*De igual manera, si la cantidad maxima
			de material introducida por el usuario no es mayor que 0, se debe emitir el error
			correspondiente y volver el menu.
			*/
			int maxEquipment = 0;
			cout << "Enter ship maximum equipment : ";
			cin >> maxEquipment;
			cin.ignore();

			if (maxEquipment <= 0) {
				error(ERR_WRONG_NUMBER);
			}
			else {
				/*
				Si ambos datos son correctos, se introducira
				la nave al nal del vector de naves del planeta, inicializando antes a 0 los campos
				"curPeople" y "curEquipment".
				*/
				Ship currentShip;
				currentShip.name = shipName;
				currentShip.maxEquipment = maxEquipment;
				currentShip.maxPeople = maxPeople;
				currentShip.curPeople = 0;
				currentShip.curEquipment = 0;
				planet.ships.push_back(currentShip);
			}
		}
	}
	else {
		/*
		Si el nombre de la nave ya existe, se emitira el ERR_WRONG_SHIP_NAME y se
		volvera al menu.Al contrario que con las bases, no es posible modificar los datos
		de una nave(pero s es posible borrarla y volverla a introducir).
		*/
		error(ERR_WRONG_SHIP_NAME);
	}
}

/*
Cuando el usuario elija una de las opciones “Delete base” o “Delete ship”,
el programa debe comprobar que hay bases o naves, respectivamente, y emitir el error ERR_NO_BASES o ERR_NO_SHIPS
si no hay naves o bases, segun corresponda, y volver al menu.
*/
void deleteBase(Planet &planet)
{
	if (planet.bases.size() > 0) {
		/*Si hay alguna base o nave(siempre segun la opcion elegida),
		se debe pedir el nombre de la base o nave con el mensaje correspondiente
		(el mismo que se usa para la introduccion de datos), y a continuacion debe
		buscar esa base o nave en el planeta.Si no se encuentra, se debe emitir el error
		ERR_WRONG_BASE_NAME o ERR_WRONG_SHIP_NAME, y volver al menu.
		*/
		string baseName = "";
		cout << "Enter base name: ";
		getline(cin, baseName, '\n');

		int baseId = BaseExist(baseName, planet);
		/*
		Si el nombre existe, se debe mostrar la informacion de la base o nave, y pedir conﬁrmacion,
		como en este ejemplo :
			[(Cargo ship D’Qar) 600{0} 12500{0}]
			Delete(Y / N) ?
		*/
		if (baseId != -1) {
			printBase(planet.bases[baseId]);
			cout << "Delete(Y/N)?: ";
			char answer = 'N';
			cin >> answer;
			cin.ignore();
			if (answer == 'Y') {
				planet.bases.erase(planet.bases.begin() + baseId);
			}
			else
			{
			}
		}
		else {
			error(ERR_WRONG_BASE_NAME);
		}
	}
	else {
		error(ERR_NO_BASES);
	}
}

void deleteShip(Planet &planet)
{
	if (planet.ships.size() > 0) {
		/*Si hay alguna base o nave(siempre segun la opcion elegida),
		se debe pedir el nombre de la base o nave con el mensaje correspondiente
		(el mismo que se usa para la introduccion de datos), y a continuacion debe
		buscar esa base o nave en el planeta.Si no se encuentra, se debe emitir el error
		ERR_WRONG_BASE_NAME o ERR_WRONG_SHIP_NAME, y volver al menu.
		*/
		string shipName = "";
		cout << "Enter ship name: ";
		getline(cin, shipName, '\n');
		int shipId = ExistShip(shipName, planet);
		/*
		Si el nombre existe, se debe mostrar la informacion de la base o nave, y pedir conﬁrmacion,
		como en este ejemplo :
		[(Cargo ship D’Qar) 600{0} 12500{0}]
		Delete(Y / N) ?
		*/
		if (shipId != -1) {
			printShip(planet.ships[shipId]);
			cout << "Delete(Y/N)?: ";
			char answer = 'N';
			cin >> answer;
			cin.ignore();
			if (answer == 'Y') {
				planet.ships.erase(planet.ships.begin() + shipId);
			}
			else
			{
			}
		}
		else {
			error(ERR_WRONG_SHIP_NAME);
		}
	}
	else {
		error(ERR_NO_BASES);
	}
}

int getAvaliableBases(Planet &planet)
{
	/*De todas las naves disponibles en el planeta, se tomara aquella que no
		tenga bases asignadas y que tenga la máxima capacidad para transportar
		personas
	*/
	vector<int> basesAvaliables;
	basesAvaliables.clear();
	for (int i = 0; i < planet.ships.size(); i++) {
		if (planet.ships[i].bases.size() == 0)
		{
			basesAvaliables.push_back(i);
		}
	}
	int baseId = -1;
	if (basesAvaliables.size() > 2) 
	{
	}
	else if (basesAvaliables.size() == 1) 
	{
		baseId = basesAvaliables[0];
	}
	return 0;
}

void evacuationPlan(Planet &planet)
{
	/*De todas las naves disponibles en el planeta, se tomara aquella que no
		tenga bases asignadas y que tenga la máxima capacidad para transportar
		personas; si hay dos o mas naves con esa misma maxima capacidad, se
		tomara la que aparezca primero en el vector de naves del planeta. Si no
		hay naves disponibles, el algoritmo terminara
	*/
	int idBase = getAvaliableBases(planet);

}

// ------------------------------------------------------
void menu()
{
	cout << "-----========== Evacuation ==========-----" << endl
		<< "1- List info" << endl
		<< "2- Add/modify base" << endl
		<< "3- Add ship" << endl
		<< "4- Delete base" << endl
		<< "5- Delete ship" << endl
		<< "6- Evacuation plan" << endl
		<< "q- Quit" << endl
		<< "Option: ";
}

//Datos de relleno para debug/test
void DatosDebug(Planet &planet) {
	Ship s1;
	s1.name = "Ship1";
	s1.curEquipment = 0;
	s1.curPeople = 0;
	s1.maxEquipment = 1000;
	s1.maxPeople = 100;

	Ship s2;
	s2.name = "Ship2";
	s2.curEquipment = 0;
	s2.curPeople = 0;
	s2.maxEquipment = 5000;
	s2.maxPeople = 125;

	Ship s3;
	s3.name = "Ship3";
	s3.curEquipment = 50;
	s3.curPeople = 100;
	s3.maxEquipment = 20000;
	s3.maxPeople = 750;

	Base b1;
	b1.name = "Base1";
	b1.people = 50;
	b1.equipment = 100;
	b1.x = 1.123;
	b1.y = 1.65;

	Base b2;
	b2.name = "Base2";
	b2.people = 88;
	b2.equipment = 178;
	b2.x = 5.23;
	b2.y = 70.65;

	planet.bases.push_back(b1);
	//planet.bases.push_back(b2);
	planet.ships.push_back(s1);
	planet.ships.push_back(s2);
	planet.ships.push_back(s3);
}

int main()
{
	Planet planet;
	planet.name = "Hoth";
	char option;

	DatosDebug(planet);

	do {
		menu();
		cin >> option; cin.ignore();

		switch (option) {
		case '1': {
			printPlanet(planet);
			break;
		}
		case '2': {
			createBase(planet);
			break;
		}
		case '3': {
			createShip(planet);
			break;
		}
		case '4': {
			deleteBase(planet);
			break;
		}
		case '5': {
			deleteShip(planet);
			break;
		}
		case '6': {
			evacuationPlan(planet);
			break;
		}
		case 'q': {
			break;
		}
		default: {
			error(ERR_UNKNOWN_OPTION);
			break;
		}
		}
	} while (option != 'q');

	return 0;
}
